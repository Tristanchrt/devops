# Mongo

https://www.bmc.com/blogs/mongodb-docker-container/#:~:text=Go%20to%20the%20“mongodb”%20folder,container%20as%20a%20background%20process.&text=The%20up%20command%20will%20pull,yml%20file.


# Increase docker ip space

/etc/docker/daemon.json

{
    "storage-driver": "devicemapper"
    "default-address-pools":[
        {"base":"172.80.0.0/16","size":24},
        {"base":"172.90.0.0/16","size":24}
    ]
}



# Docker-compose 

version: "2.2"

networks:
    aciso_dev:
        ipam:
            driver: default
            config:
                - subnet: 172.16.154.0/24
                  gateway: 172.16.154.1

services:
  db:
    restart: always
    image: postgres:11
    volumes:
      - /srv/docker/aciso/dev/db:/var/lib/postgresql/data
      - ./conf/postgres:/etc/postgres/conf.d
      - ./postgres_init.sh:/docker-entrypoint-initdb.d/aciso_init.sh
    ports:
      - "127.0.0.1:5433:5432"
    environment:
      - POSTGRES_USER=aciso
      - POSTGRES_PASSWORD=NfrUzZ6w9368j6rxX9yE
      - POSTGRES_DB=aciso
    container_name: aciso_dev_db
    networks:
      - aciso_dev

#  dhtmlx_export:
#    restart: always
#    image: dhtmlx/scheduler-gantt-export
#    ports:
#      - 5420:80
#    container_name: aciso_dhtmlx_export
#    volumes:
#      - /srv/docker/aciso/dev/dhtmlx_export:/data/export dhtmlx/scheduler-gantt-export
#    networks:
#      - aciso_dev

  web:
    image: antoinescherrer/aciso:may21
    restart: always
#    build: ./web
    container_name: aciso_dev_web
    ports:
      - "127.0.0.1:5481:8000"
    volumes:
      - ./web/src:/usr/src/app
      - ./web/src/config_dev_docker.py:/usr/src/app/config.py
      - /srv/docker/aciso/dev/web:/data/aciso
      - ./data:/aciso_data:ro
    depends_on:
      - db
      - celery
      - redis
      - athenapdf
      - yake
      - mailhog
    command: /usr/local/bin/gunicorn -c gunicorn_dev.conf.py app:app --reload
    logging:
      driver: "json-file"
      options:
        max-size: "2M"
        max-file: "5"
    networks:
      - aciso_dev

  redis:
    restart: always
    image: library/redis:5-alpine
    container_name: aciso_dev_redis
    ports:
      - "127.0.0.1:6379:6379"
    volumes:
     - /srv/docker/aciso/dev/redis:/data
    environment:
      - ALLOW_EMPTY_PASSWORD=yes
    command: redis-server --appendonly no
    networks:
      - aciso_dev

  celery:
    restart: always
    image: antoinescherrer/aciso:may21
#    build: ./web
    container_name: aciso_dev_celery
    volumes:
      - ./web/src:/usr/src/app
      - ./web/src/config_dev_docker.py:/usr/src/app/config.py
      - /srv/docker/aciso/dev/web:/data/aciso
    depends_on:
     - rabbitmq
    command: celery worker -A celery_worker.celery -l info -Q celery,rl,ind
    networks:
      - aciso_dev

  rabbitmq:
    restart: always
    container_name: aciso_dev_rabbitmq
    environment:
      - RABBITMQ_DEFAULT_USER=guest
      - RABBITMQ_DEFAULT_PASS=guest
    ports:
      - "127.0.0.1:15673:15672"
    image: rabbitmq:3.8-management-alpine
    networks:
      - aciso_dev

  cli:
    image: antoinescherrer/aciso:may21
#    build: ./web
    container_name: aciso_dev_cli
    volumes:
      - ./web/src:/usr/src/app
      - ./web/src/config_dev_docker.py:/usr/src/app/config.py
      - /srv/docker/aciso/dev/web:/data/aciso
      - ./data:/aciso_data:ro
    depends_on:
      - db
    #    command: /usr/local/bin/python3 cli.py
    command: /usr/local/bin/python3 cli.py
    networks:
      - aciso_dev
    logging:
      driver: "json-file"
      options:
        max-size: "2M"
        max-file: "5"

  athenapdf:
    image: arachnysdocker/athenapdf-service
    container_name: aciso_dev_athenapdf
    environment:
      - WEAVER_AUTH_KEY=aciso_dev
      - WEAVER_ATHENA_CMD=athenapdf -S
    networks:
      - aciso_dev

  yake:
    image: excube-yake-server
    build: ./yake
    ports:
      - "127.0.0.1:5490:5000"
    container_name: aciso_dev_yake
    networks:
      - aciso_dev

  mailhog:
      restart: always
      image: mailhog/mailhog
      ports:
        - "127.0.0.1:1125:1025"
        - "127.0.0.1:8025:8025"
      container_name: aciso_dev_mailhog
      networks:
        - aciso_dev
