# La Palette

version: '3.7'
services:
  db:
    container_name: palette_db
    image: mongo:latest
    restart: always
    environment:
      MONGO_INITDB_DATABASE: admin
      MONGO_INITDB_ROOT_USERNAME: admin
      MONGO_INITDB_ROOT_PASSWORD: 123+aze
    ports:
      - 27017:27017
    volumes:
        - ./docker/volumes/db/data/db/:/data/db/
        # - ./docker/volumes/db/mongod.conf:/etc/mongod.conf
        - ./docker-entrypoint-initdb.d/mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro
    #command: ["-f", "/etc/mongod.conf"]