mongo -- "palette_db" <<EOF
    var rootUser = 'palette';
    var rootPassword = 'palette';
    var admin = db.getSiblingDB('admin');
    admin.auth(rootUser, rootPassword);

    var user = 'palette';
    var passwd = 'palette';
    db.createUser({user: user, pwd: passwd, roles: ["readWrite"]});
EOF