from flask import *
from redis import Redis
from celery import Celery
import config as conf

app = Flask(__name__)
redis = Redis(host=conf.REDIS_HOST, port=conf.REDIS_PORT)

celery = Celery(
    'tasks', broker='amqp://'+conf.CELERY_USER+':'+conf.CELERY_USER+'@'+conf.RABBIT_HOST+'//',  
            backend='redis://'+conf.REDIS_HOST+':'+conf.REDIS_PORT+'/0')

@celery.task()
def add(x=3, y=8):
    return x + y

@app.route('/')
def video_feed():
    redis.set('mykey', 'Hello from Python!')
    value = redis.get('mykey') 
    return f'Hello from flask : {value} and add : {add()} message : {conf.MESSAGE}'

@app.route('/home')
def home():
    return "Hello world"
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
