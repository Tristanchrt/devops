# import cli.app
# import click
# import os

# @cli.app.CommandLineApp
# def ls(app):
#     print("Hello world!")
#     pass

# ls.add_param("-l", "--long", help="list in long format", default=False, action="store_true")

# if __name__ == "__main__":
#     ls.run()


# cli.py

import click

# py .\cli.py oui --count 3 --name pomme

@click.group()
def item():
    pass

@click.group()
def customer():
    pass

@item.command()
@click.argument('value')
@click.option('--count', default=1, help='Number of greetings.')
@click.option('--name', help='The person to greet.')
def hello(value, count, name):
    for x in range(count): click.echo(f"Hello {name} {value}!")

@item.command()
@click.argument('value')
def display(value,):
    click.echo(f"Display {value}!")

@customer.command()
@click.argument('value')
def display(value,):
    click.echo(f"Display {value}!")


if __name__ == '__main__':
    @click.group()
    def cli():
        pass

    cli.add_command(customer)
    cli.add_command(item)

    cli()

    # py .\cli.py customer display eola
    # py .\cli.py item display eola