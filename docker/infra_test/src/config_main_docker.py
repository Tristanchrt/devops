# Redis
REDIS_HOST = 'palette_redis_main'
REDIS_PORT = '6379'

# Web
WEB_HOST = 'palette_web_main'

# Rabbit
RABBIT_HOST = 'palette_rabbit_main'

# Celery
CELERY_WORKER = 'palette_celery_main'
CELERY_HOST = 'rabbitmp'
CELERY_USER = 'gest'
CELERY_PASSWORD = 'gest'

MESSAGE = "MAIN"