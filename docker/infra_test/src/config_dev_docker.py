# Redis
REDIS_HOST = 'palette_redis_dev'
REDIS_PORT = '6379'

# Web
WEB_HOST = 'palette_web_dev'

# Rabbit
RABBIT_HOST = 'palette_rabbit_dev'

# Celery
CELERY_WORKER = 'palette_celery_dev'
CELERY_HOST = 'rabbitmp'
CELERY_USER = 'gest'
CELERY_PASSWORD = 'gest'

MESSAGE = "DEV"