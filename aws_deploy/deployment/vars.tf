variable "AWS_REGION" {    
    default = "eu-west-1"
}
variable "AMI" {
    default = "ami-0a8e758f5e873d1c1"
}
variable "INSTANCE_TYPE" {
    default = "t2.micro"
}
variable "PRIVATE_KEY_PATH" {
  default = "~/.ssh/id_rsa"
}
variable "PRIVATE_KEY_PATH_PEM" {
  default = "~/Documents/dev/strapi_test/deployment/user@laptop-cpe-.pem"
}
variable "PUBLIC_KEY_PATH" {
  default = "~/.ssh/id_rsa.pub"
}
variable "EC2_USER" {
  default = "ubuntu"
}
variable "INSTANCE_NAME" {
  default = "strapi_dev"
}
