resource "aws_internet_gateway" "prod-igw" {
    vpc_id = "${aws_vpc.test_strapi.id}"
    tags = {
        Name = "prod-igw"
    }
}