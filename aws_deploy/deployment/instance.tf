locals {
  variables = {
    dev-environment = {
      instance_type          = "t2.micro",
      ami                    = var.AMI
      subnet_id              = aws_subnet.prod-subnet-public-1.id,
      vpc_security_group_ids = ["${aws_security_group.ssh-allowed.id}"]
      key_name               = aws_key_pair.id_rsa.id
      environment            = "dev"
      tags = {
        Name = "dev"
      }
    },
    test-environment = {
      instance_type          = "t2.nano",
      ami                    = var.AMI,
      subnet_id              = aws_subnet.prod-subnet-public-1.id,
      vpc_security_group_ids = ["${aws_security_group.ssh-allowed.id}"]
      key_name               = aws_key_pair.id_rsa.id
      environment            = "test"
      tags = {
        Name = "test"
      }
    }
  }
}


